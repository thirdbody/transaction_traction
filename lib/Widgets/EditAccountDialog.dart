import 'package:flutter/material.dart';
import 'package:transaction_traction/Widgets/HomePage.dart';
import 'package:transaction_traction/classes/Account.dart';
import 'package:transaction_traction/storage.dart';
import 'package:uuid/uuid.dart';

class EditAccountDialog extends StatefulWidget {
  EditAccountDialog({
    Key? key,
    required this.parent,
    this.defaultValue,
  }) : super(key: key);

  final GlobalKey<HomePageState> parent;
  final Account? defaultValue;

  @override
  EditAccountDialogState createState() => EditAccountDialogState();
}

class EditAccountDialogState extends State<EditAccountDialog> {
  final nameController = TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    nameController.text = widget.defaultValue?.name ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.defaultValue?.name ?? 'New Account'),
      ),
      body: _content(context),
    );
  }

  ListView _content(BuildContext context) => ListView(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 12,
              bottom: 16,
              left: 15,
              right: 15,
            ),
            child: TextField(
              decoration: InputDecoration(hintText: 'Name'),
              controller: nameController,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 40, right: 40, bottom: 20, top: 20),
            child: ElevatedButton(
              onPressed: () {
                var a = Account(
                  uuid: Uuid().v4(),
                  name: nameController.text,
                  transactions: {},
                );

                updateAccount(a).then((accounts) {
                  widget.parent.currentState!.refresh(accounts.toList());
                  Navigator.of(context).pop();
                });
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      );
}
