import 'package:flutter/material.dart';
import 'package:transaction_traction/Widgets/CategoryBreakdown.dart';
import 'package:transaction_traction/Widgets/EditAccountDialog.dart';
import 'package:transaction_traction/Widgets/TransactionList.dart';
import 'package:transaction_traction/classes/Account.dart';
import 'package:transaction_traction/classes/RefreshableWidget.dart';
import 'package:transaction_traction/classes/Transaction.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key? key,
    required this.accounts,
    required this.parent,
  }) : super(key: key);

  final List<Account> accounts;
  final GlobalKey parent;

  @override
  HomePageState createState() => HomePageState(accounts: accounts);
}

class HomePageState extends RefreshableWidgetState<HomePage, List<Account>> {
  HomePageState({required List<Account> accounts}) : _accounts = accounts;

  List<Account> _accounts;

  void refresh(List<Account> accounts) => setState(() => _accounts = accounts);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Accounts'),
          actions: [_addAccountButton(context)],
        ),
        floatingActionButton: _categoryBreakdownButton(context),
        body: _content(context),
      );

  Widget _content(BuildContext context) => ListView.separated(
        itemCount: _accounts.length,
        itemBuilder: _account,
        separatorBuilder: (context, int) => const Divider(),
      );

  ListTile _account(BuildContext context, int index) => ListTile(
        title: Text(_accounts[index].name),
        subtitle: Text(_accounts[index].getBalance().toStringAsPrecision(4)),
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => TransactionList(
              key: GlobalKey<TransactionListState>(
                  debugLabel: 'Transaction list'),
              title: _accounts[index].name,
              transactions: _accounts[index].transactions.toList(),
              parent: widget.key! as GlobalKey<HomePageState>,
              account: _accounts[index],
            ),
          ),
        ),
      );

  FloatingActionButton _categoryBreakdownButton(BuildContext context) =>
      FloatingActionButton(
        child: Icon(Icons.analytics_outlined),
        onPressed: () {
          Set<Transaction> allTransactions = _accounts.fold(
            Set(),
            (Set<Transaction> previous, Account current) =>
                previous.union(current.transactions),
          );

          Map<String, List<Transaction>> categories = {};
          allTransactions.forEach((element) {
            element.categories.forEach((key, value) {
              var transaction = Transaction(
                uuid: element.uuid,
                amount: value,
                description: element.description,
                merchant: element.merchant,
                date: element.date,
                categories: element.categories,
                account: element.account,
              );
              if (categories.containsKey(key.toUpperCase())) {
                categories[key.toUpperCase()] =
                    categories[key.toUpperCase()]! + [transaction];
              } else {
                categories.addAll({
                  key.toUpperCase(): [transaction],
                });
              }
            });
          });
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CategoryBreakdown(
                key: GlobalKey<CategoryBreakdownState>(
                    debugLabel: 'Category breakdown'),
                categories: categories,
                parent: widget.key! as GlobalKey<HomePageState>,
              ),
            ),
          );
        },
      );

  IconButton _addAccountButton(BuildContext context) => IconButton(
        icon: Icon(Icons.add),
        onPressed: () => showDialog(
          context: context,
          builder: (context) => EditAccountDialog(
            key: GlobalKey(debugLabel: 'Create account dialog'),
            parent: widget.key! as GlobalKey<HomePageState>,
          ),
        ),
      );
}
