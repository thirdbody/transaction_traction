import 'package:json_annotation/json_annotation.dart';
import 'package:transaction_traction/classes/Transaction.dart';
import 'package:transaction_traction/storage.dart';

part 'Account.g.dart';

@JsonSerializable(explicitToJson: true)
class Account {
  Account({required this.uuid, required this.name, required this.transactions});

  String uuid;
  String name;
  Set<Transaction> transactions;

  factory Account.fromJson(Map<String, dynamic> json) =>
      _$AccountFromJson(json);

  Map<String, dynamic> toJson() => _$AccountToJson(this);

  double getBalance() {
    return transactions.fold(
      0.0,
      (previousValue, element) => previousValue + element.amount,
    );
  }

  void updateOrAddTransaction(Transaction t) {
    if (transactions.any((element) => element.uuid == t.uuid)) {
      transactions
          .remove(transactions.firstWhere((element) => element.uuid == t.uuid));
    }
    transactions.add(t);
    updateAccount(this);
  }
}
