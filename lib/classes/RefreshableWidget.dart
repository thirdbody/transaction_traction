import 'package:flutter/widgets.dart';

abstract class RefreshableWidgetState<T extends StatefulWidget, V>
    extends State<T> {
  void refresh(V v);
}
