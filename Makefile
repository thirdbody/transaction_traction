.PHONY: build json analyze apk

build: json analyze apk

json:
	flutter pub run build_runner build

analyze:
	flutter analyze .

apk:
	flutter build apk
